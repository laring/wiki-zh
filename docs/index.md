# 欢迎访问 OpenBlock Wiki

## 简介

OpenBlock 是一个开源图形块编程软件，即可以用于软件程序编程也可以对硬件设备编程，支持硬件代码生成及编译下载功能，同时也支持通过与硬件设备间的实时通讯实现的实时运行模式。

![openblock](./assets/screenshoot1.png)

## 硬件支持列表

| 设备类型    | 设备型号                                                     | 支持性           |
| ----------- | ------------------------------------------------------------ | ---------------- |
| Arduino     | Arduino UNO, Arduino Nano, Arduino Mega256, Arduino Leonardo | 当前支持         |
| MicroPython | Microbit, Maixduino                                          | 在下个版本中支持 |

## 快速开始

[我是普通用户](i-am-normal-user.md){: .md-button .md-button--primary }
[我是软件开发者](software-developer/quick-start.md){: .md-button .md-button--primary }



