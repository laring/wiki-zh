# 打包离线版或服务器部署

## 打包离线版

1. 首先clone离线版工程

    ```bash
    git clone https://github.com/openblockcc/openblock-desktop
    cd openblock-desktop
    ```

2. 安装依赖包

    ```bash
    npm i
    ```

3. 打包

    ```bash
    npm run dist
    ```

4. 等待electron运行完毕后即可在工程文件夹下的dist文件夹中看到打包完毕的软件了

## 服务器部署

在服务器部署时，为了使得网页可以访问到客户机上的硬件资源，同时编译和上传程序也需要在客户机运行，所以客户机上需要安装运行OpenBlock Link程序。所以首先我们需要打包OpenBlock Link。

1. 与离线版相同，我们需要clone OpenBlock Link工程，安装依赖包并打包。

    按照顺序挨个执行以下指令，就可以生成openblock-link的安装包了

    ```bash
    git clone https://github.com/openblockcc/openblock-link-desktop
    cd openblock-link-desktop
    npm i
    npm run build:dist
    ```

2. 而后我们clone openblock-gui 并完成编译，而后将编译出的文件部署在自己的服务器上即可。

3. 在客户访问在线网站时，只需要提起开启OpenBlock Link客户端即可使用了。
