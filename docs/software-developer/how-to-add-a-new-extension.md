# 如何添加新的设备扩展

## 概述

在 OpenBlock 中 devices extension 是为device设备提供的扩展，不同于scratch原生扩展，devices extension不会被webpack打包，而将由一个本地服务器程序通过websock提供给 VM。通过这种方式我们就可以实现扩展的动态修改和加载功能了。

以下所有工作将在 **openblock-extension** 中完成。

## 具体步骤

在 `openblock-extension\src` 下复制dht11为一个新的文件夹，并将其改名为你的扩展的名字。

而后修改下面这些内容来实现你的扩展功能

- [src/&lt;new extension&gt;/index.js](#indexjs) 插件基本信息
- [src/&lt;new extension&gt;/blocks.js](#blocksjs) 插件积木块定义
- [src/&lt;new extension&gt;/msg.js](#msgjs) 多语言支持
- [src/&lt;new extension&gt;/generator.js](#generatorjs) 代码生成器规则
- [src/&lt;new extension&gt;/toolbox.js](#toolboxjs) 左侧工具栏积木排列和默认值
- [src/locales.js](#localesjs) 插件基本信息多语言支持
- [src/lib](#lib) 代码库文件

### index.js

index.js文件的内容描述了此插件的名称，版本，作者等显示内容，也描述了此插件的积木渲染，代码转译等函数文件的位置。下面以dht11的文件举例。

```js
const dht11 = formatMessage => ({
    name: 'DHT11',
    extensionId: 'dht11',
    version: '1.0.0',
    type: 'arduino',
    supportDevice: ['arduinoUno', 'arduinoNano', 'arduinoLeonardo', 'arduinoMega2560'],
    author: 'ArthurZheng',
    iconURL: `asset/DTH11.png`,
    description: formatMessage({
        id: 'dht11.description',
        default: 'DHT11 Temperature and humidity sensor module.',
        description: 'Description of dht11'
    }),
    featured: true,
    blocks: `blocks.js`,
    generator: `generator.js`,
    toolbox: `toolbox.js`,
    msg: `msg.js`,
    location: 'local', // or 'remote'
    tags: ['sensor'],
    helpLink: 'https://www.baidu.com'
});

module.exports = dht11;
```

*以下带有星号\*的参数为必须参数*

- **name***

    显示在扩展选择界面的名字

- **extensionId***

    扩展的唯一ID

- **type***

    标识对此插件的处置方式

- **version**

    显示在扩展选择界面的版本号

- **supportDevice***

    支持的设备，扩展只会选择了对应支持的设备后才会在扩展选择界面显示

- **author**

    显示在扩展选择界面的作者名字

- **iconURL***

    显示在扩展选择界面的图片，图片比例和像素参数请参照DHT11样例中的修改

- **description**

    显示在扩展选择界面图片下方扩展介绍内容

- **featured***

    此参数为内部参数，必须设置为true

- **blocks***

    扩展的积木渲染生成代码

- **generator***

    扩展的积木的转转译代码

- **toolbox***

    扩展的工具箱代码，描述扩展在界面左侧工具箱中的显示和排列方式

- **msg***

    扩展的多语言支持

- **location**

    只有 local / remote 两个选项，用来标识扩展是部署在云端还是本地，在目前的版本中，直接默认为local 即可，此参数可能在之后将会弃用。

- **tags***

    标识此插件的类目。可选选项有：actuator, sensor, display, communication, other。

- **helpLink**

    帮助链接，点击插件页中的帮助将打开此链接。

### blocks.js

此文件定义了插件内部的积木的样式，以下为DHT样板的代码片段。

```js
// src\dht11\blocks.js
var color = '#28BFE6';

Blockly.Blocks['DHT_INIT'] = {
    init: function () {
        this.jsonInit({
            "message0": Blockly.Msg.DHT_INIT,
            "args0": [{
                "type": "input_value",
                "name": "no"
            }, {
                "type": "field_dropdown",
                "name": "pin",
                "options": [
                    ['0', '0'],
                    ['1', '1'],
                    ['2', '2'],
                    ['3', '3'],
                    ['4', '4'],
                    ['5', '5'],
                    ['6', '6'],
                    ['7', '7'],
                    ['8', '8'],
                    ['9', '9'],
                    ['10', '10'],
                    ['11', '11'],
                    ['12', '12'],
                    ['13', '13'],
                    ['A0', 'A0'],
                    ['A1', 'A1'],
                    ['A2', 'A2'],
                    ['A3', 'A3'],
                    ['A4', 'A4'],
                    ['A5', 'A5']]
            }, {
                "type": "field_dropdown",
                "name": "mode",
                "options": [
                    ['dht11', '11'],
                    ['dht21', '21'],
                    ['dht22', '22']]
            }],
            "colour": color,
            "extensions": ["shape_statement"]
        });
    }
};

// src\dht11\msg.js line 4
DHT_INIT : 'init dht %1 pin %2 mode %3',
```

一个积木块由以下参数构成

**message0**：

    积木整体显示内容由message0内容决定，由于这里由多语言翻译内容，所以这里的内容是由 msg.js 中定义的，可以看到积木中的参数是由 %1 %2这样的形式定义的。而后其参数的具体内容由args0定义的数组定义。

**args0** 属性：

- **type**

    可选值：

        **input_value**：输入框，默认为圆角矩形，即数字与字符串接收框，如果下方check属性的值为Boolean，则此处为尖角矩形，即布尔值接收框。

        **field_dropdown**：下拉框，选项内容在option中定义。

        **math_angle**：输入框，数字与字符串接收框，蛋点击后会弹出一个角度界面，可以以图形化方式设定此输入框数值。

        ... 仍有很多选项，待补充

- **name**

    参数名称，在代码转译函数中会根据这个名称来获取次参数的值。

- **check**

    可选值：

        **Boolean**：定义此参数框仅接收布尔值。

        **Number**：定义此参数框仅接收数字值。

- **options**

    定义下拉框的内容，数组的第一个值为实际显示内容，第二个值为代码转译时获取的值。

**colour**：

    积木颜色，一般一个扩展我们通常都使用同一个颜色，所此处以一个颜色变量引入。

**extensions**：

    这个参数决定了，积木的类型或这说是外部形状。可选值有：

    **shape_statement**：可以上下连接积木的常规命令方块

    **output_number**：圆角矩形积木，输出数字值

    **output_string**：圆角矩形积木，输出字符串

    **output_boolean**：尖角矩形积木，输出布尔值

    ... 还有一些选项，待补充

### msg.js

用来实现多语言支持功能，定义了Block名称以及目录的实际显示的内容。

```js
function addMsg (Blockly) {
    Object.assign(Blockly.ScratchMsgs.locales.en, {
        DHT_CATEGORY: 'DHT',
        DHT_INIT: 'init dht %1 pin %2 mode %3',
        DHT_READ_TEMPERATURE: 'dht %1 read temperature %2',
        DHT_READ_HUMIDITY: 'dht %1 read humidity'
    });
    Object.assign(Blockly.ScratchMsgs.locales['zh-cn'], {
        DHT_CATEGORY: 'DHT',
        DHT_INIT: '初始化 dht %1 引脚 %2 型号 %3',
        DHT_READ_TEMPERATURE: 'dht %1 读取温度 %2',
        DHT_READ_HUMIDITY: 'dht %1 读取湿度'
    });
    return Blockly;
}

exports = addMsg;

```


### generator.js

此文件定义了插件积木的代码转译函数。

```js
Blockly.Arduino['DHT_INIT'] = function (block) {
    let no = Blockly.Arduino.valueToCode(this, 'no', Blockly.Arduino.ORDER_ATOMIC);
    let pin = this.getFieldValue('pin');
    let mode = this.getFieldValue('mode');

    Blockly.Arduino.includes_['include_DHT_INIT'] = `#include "<DHT.h>"`;
    Blockly.Arduino.definitions_['define_DHT_INIT_${no}'] = `DHT dht_${no}(${pin}, ${mode});`;
    return '';
}

Blockly.Arduino['DHT_READ_HUMIDITY'] = function (block) {
    let no = Blockly.Arduino.valueToCode(this, 'no', Blockly.Arduino.ORDER_ATOMIC);
    return [`dht_${no}.readHumidity()`, Blockly.Arduino.ORDER_ATOMIC];
}
```

在转译函数中，有两个函数方式来获取积木内部参数：

- **Blockly.Arduino.valueToCode**：

​   此函数用于获取积木内部的可以接收其他积木输入的参数内容（由于这类输入框中可能会放置其他积木，使用该函数会继续调用解析其内容积木内容）。函数第一个参数输入这个积木本身即this，第二个参数为此输入框的名称，第三个参数为参数优先级，直接默认使用Blockly.Arduino.ORDER_ATOMIC即可。

- **this.getFieldValue**

​   此函数用于获取积木内部下拉框参数的内容

积木代码的组成有两个部分，一是定义在开头位置的include等内容，二是在积木本身位置生成的代码内容。对于前者：

- **Blockly.Arduino.includes_、Blockly.Arduino.definitions_、Blockly.Arduino.definitions_、Blockly.Arduino.setups_、Blockly.Arduino.loops_**

    为这几个变量赋值，会在代码对应的位置生成定义的内容。注意这里为数组形式，相同名称的数组内容不会重复生成。放置拖拽同一个积木时生成重复的代码内容。

而对于后者，积木本身位置的代码需要在return中返回。如果积木是一个输出类型，还需要在返回时返回一个代码优先级，此处直接使用Blockly.Arduino.ORDER_ATOMIC即可。

### toolbox.js

此文件定义了积木在toolbox中的显示内容。以下为DHT样板的代码片段。

```xml
function addToolbox () {
    
    const dhtIconUrl = 'data:image/svg+xml;base64,XXXXXXXXXXXXXXXX'

    return `
<category name="%{BKY_DHT_CATEGORY}" id="DHT_CATEGORY" colour="#42CCFF" secondaryColour="#42CCFF" iconURI="${dhtIconUrl}">
    <block type="dht_init" id="dht_init">
        <value name="no">
            <shadow type="math_number">
                <field name="NUM">1</field>
            </shadow>
        </value>
        <value name="pin">
            <shadow type="math_number">
                <field name="NUM">2</field>
            </shadow>
        </value>
    </block>
    <block type="dht_readHumidity" id="dht_readHumidity">
        <value name="no">
            <shadow type="math_number">
                <field name="NUM">1</field>
            </shadow>
        </value>
    </block>
    <block type="dht_readTemperature" id="dht_readTemperature">
        <value name="no">
            <shadow type="math_number">
                <field name="NUM">1</field>
            </shadow>
        </value>
    </block>
</category>`;
}

exports = addToolbox;
```

此处目录的名称为了多语言翻译，调用了msg中DHT_CATEGORY的值，不过在这里使用时需要在前方添加`BKY_`

而后下方就是之前我们定义的积木的结构，不过需要注意的是，这里我们还需要对积木内部的input_value赋初始值，即代码片段中block内部的value内容。积木下拉菜单中默认值是排第一个的选项，如果我们需要设定该默认值，直接在block结构下设定field即可，如下所示我们对dht11初始化积木的mode选项设定其值为22，这里需要使用实际值而非显示值。

```xml
<block type="DHT_INIT" id="DHT_INIT">
    <value name="no">
        <shadow type="math_number">
            <field name="NUM">1</field>
        </shadow>
    </value>
    <field name="mode">22</field>
</block>
```

### locales.js

此文件也是多语言支持功能的一部分，但仅在index文件中使用，是index文件中formatMessage的翻译文本。

```js
module.exports = {
    'en': {
        'l298n.description': 'L298N motor drive module.',
        'dht11.description': 'DHT11 temperature and humidity sensor module.',
        'hc-sr04.description': 'HC-SR04 distance measurement module.',
        'oled.description': 'I2C oled display.',
        'softwareSerial.name': 'Software serial',
        'softwareSerial.description': 'Allow serial communication on other digital pins of the Arduino.',
        'cooperativeScheduler.name': 'Cooperative scheduler',
        'cooperativeScheduler.description': 'Allow Arduino run multiple applications.'
    },
    'zh-cn': {
        'l298n.description': 'L298N 电机驱动模块.',
        'dht11.description': 'DHT11 温湿度传感器模块.',
        'hc-sr04.description': 'HC_SR04 超声波测距模块.',
        'oled.description': 'I2C oled 显示屏.',
        'softwareSerial.name': '软件串口',
        'softwareSerial.description': '使Arduino可以使用其他数字口进行串口通信.',
        'cooperativeScheduler.name': '多任务调度器',
        'cooperativeScheduler.description': '使Arduino可以运行多个应用.'
    }
};
```

### lib

插件文件夹下的lib文件用来放置编译插件代码是需要的库文件。如在dht11插件下，在lib文件中就放置了DHT_sensor_library的Arduino库。

## 测试插件

完成修改后我们重启link服务器，而后在GUI界面中即可测试这个插件了。
