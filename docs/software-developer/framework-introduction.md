## 构架介绍

## 简介

工程整体以NodeJS开发，其中的代码以npm包的方式根据功能做分割。最终被整理调用封包为面向客户的软件内容。构架根据软件部署方式分为在线版与离线版两种。但他们都使用了大量共有的软件包。

- openblock-gui 基于React的前端用户界面。
- openblock-vm 管理状态和运行后台逻辑代码，接收GUI的操作请求并发送状态和事件给GUI。
- openblock-blocks 从Google [Blockly](https://developers.google.com/blockly/) 项目创建的分支。这个仓库同时处理实现积木块（blocks）的UI显示、逻辑功能以及代码生成功能，它通过vm被gui调用。
- openblock-link 通过websocket为openblock提供本地硬件接口服务，与本地运行的编译软件服务。
- openblock-extension 通过websocket为openblock提供本地插件内容。

## 在线版

在线版部署方式是通过在服务器端部署 openblock-gui 内容，而后在用户端安装 openblock-link-desktop 来实现完整功能。其整体结构分为两个部分，他们之间通过websock来连接：

- openblock-gui

	核心依赖包: openblock-vm, openblock-blocks

- openblock-link-desktop

	核心依赖包: openblock-link, openblock-extension

## 离线版

在离线版的情形下，所有软件包都通过electron一起打包为一个本地独立运行的软件。

- openblock-desktop

	核心依赖包: openblock-vm, openblock-blocks, openblock-link, openblock-extension