# 开始开发 OpenBlock

## 开发环境

请确保你的计算机已经安装好了以下软件或工具。

- Node > 8 (推荐使用版本10，过高的Node版本在某些情况下也会导致编译失败) 
- python2.7
- Git
- Java

!!! Warning
    如果你是在中国地区或其他网络受限地区，你可能需要购买一个网络代理才能够成功执行下面的操作内容。通过以下指令设置npm的网络代理，将下面的server 和 port 替换为你的代理地址和端口。

    ```bash
    npm config set proxy http://server:port
    npm config set https-proxy http://server:port
    ```

## 克隆&安装

1. 克隆所有工程核心仓库。 (使用```--depth``` 指令来避免下载全部的仓库历史内容，否则GUI的大小将会超过1.18GB)

    ```bash
    mkdir openblock
    cd openblock
    git clone --depth 3 https://github.com/openblockcc/openblock-gui
    git clone --depth 3 https://github.com/openblockcc/openblock-blocks
    git clone --depth 3 https://github.com/openblockcc/openblock-vm
    git clone https://github.com/openblockcc/openblock-link
    git clone https://github.com/openblockcc/openblock-extension
    ```

2. 安装和链接

    - `install` 指令会根据 package.json 内容下载安装所有必要的npm包
    - `link` 指令会以本地npm包替换项目内默认的包，这样你在openblock-blocks 等包中的修改才会被 openblock-gui 使用，否则它默认使用的将会是从npm服务器下载的内容，你可以查看openblock-gui/node_modules 下的 openblock-blocks 与 openblock-vm 的文件夹，如果 link 成功，那么他们将被链接到本地克隆下来的工程的位置。

    ```bash
    cd openblock-blocks
    npm install
    npm link
    cd ..
    cd openblock-vm
    npm install
    npm link
    cd ..
    cd openblock-gui
    npm install
    npm link openblock-blocks openblock-vm
    cd ..
    cd openblock-link
    npm install
    cd ..
    cd openblock-extension
    npm install
    cd ..
    ```

3. 复制 Arduino IDE 到路径： `openblock-link\tools\Arduino\*`，完成后的文件树结构将如下面所示。(请按照下方结构严格遵循路径名称放置)

    ```bash
    openblock-link\tools\
    |- Arduino\
      |- drivers\
      |- examples\
      |- hardware\
      |- java\
      ...
      |- arduino.exe
      |- arduino_debug.exe
    ```

## 运行

1. 启动硬件接口服务器 openblock-link

    ```bash
    cd openblock-link
    npm start
    ```

2. 启动插件内部服务器 openblock-extension

    在新的一个终端中执行。

    ```bash
    cd openblock-extension
    npm start
    ```

3. 启动 openblock-gui

    在新的一个终端中执行。

    ```bash
    cd openblock-gui
    npm run start-open
    ```

4. 在 openblock-gui 的 webpack 构建完成后，将会有弹出 openblock-gui 的网页，开始使用吧。当然你也可以通过手动输入地址 `http://127.0.0.1:8601/` 来访问。

## 了解更多开发内容

1. [构架介绍](framework-introduction.md) 
2. [如何添加新的设备](how-to-add-a-new-device.md)
3. [如何添加新的扩展](how-to-add-a-new-extension.md)
4. [打包离线版或服务器部署](pack-desktop-and-server-deployment.md)